﻿// Сервис для хранения и управления стеком групп ОГВ, инжектится в директивы плиток и стека. 
// Обновление данных запускается при получении события 'changeStack'(ОГВ), по завершению генерируется 'newStack'.

(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('_stack', factory);

    factory.$inject = ['$rootScope','$q','_storage','apiHelper'];
    function factory($rootScope, $q, _storage, apiHelper) {
        var canceller = null;   // содержит промис для отмены запроса
        var params = null;      // объект с параметрами запроса, сюда вкладывается отменяющий промис
        var busy = false;       // флаг, сиглализирующий о том, что сервис находится в процессе получения данных
        var def = false;        // при вызове сервис отдает промис, который резолвится после того, как флаг busy станет false
        var address = 'apiex/govdepartment/getparents/';
        var service = {
            module:'list',
            stack: _storage.have('list','stack') ? _storage.get('list','stack') : [],
            get:get     // Возвращает промис, который рзолвится после окончания обновления данных
        };

        // Построить новый стек родителей для выбранной группы ОГВ
        $rootScope.$on('changeStack', function (event, group) {
            if (typeof group === 'undefined') {
                console.error('Параметр не может быть пустым!');
                return false;
            } else if (group === null || group.Id === null) {
                service.stack = [];
                _storage.set(service.module, 'stack', service.stack); // Скопировать в хранилище
                $rootScope.$broadcast('newStack');
            } else {
                busy = true; // Заблокировать сервис
                getStack(group, function (stack) {
                    service.stack = stack;
                    _storage.set(service.module, 'stack', service.stack); // Скопировать в хранилище
                    def && def.resolve(service.stack); // Зарезолвить новыми данными, если def уже не пустой
                    busy = false; // Разблокировать сервис
                    $rootScope.$broadcast('newStack');
                });
            }
        });

        return service;

        function get() {
            def = $q.defer();
            !busy && def.resolve(service.stack); // Если сервис не занят, то сразу зарезолвить 
            return def.promise;
        }

        /* Приватные функции */
        
        function getStack(govDep, callback) {
            if(!_.isFunction(callback) || _.isEmpty(govDep)){
                console.error('Ошибочные параметры!');
                return false;
            }
            var uri;
            canceller && canceller.resolve(); // Отменить предыдущий запрос 
            canceller = $q.defer(); // Создать новый отменяющий промис
            params = { timeout: canceller.promise }; // Вложить отмняющий промис в параметры запроса
            uri = address + govDep.Id;
            apiHelper.apiGet(uri, params, function (result) {
                var stack = result.data;
                if (typeof stack === 'object') {
                    stack = stack.reverse();
                    // Сохраняем полученные данные в сервисе:
                    //if (dep.HasChildren === false) {
                        // Если это конечный ОГВ, то устанавливаем его в качестве активной организации
                    //    service.setActiveGovDep(dep);
                    //} else {
                        // иначе добавляем в конец стека и очищаем активную организацию
                    //    service.setActiveGovDep(null);
                    stack.push(govDep);
                    //}
                    //service.govDepStack = stack;                    
                    callback(stack);
                } else {
                    console.error("Не могу получить список департаментов от сервера!");
                }

            });
        }// Формирование нового стека родительских организаций 
    }
})();