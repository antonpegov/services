// Сервис для управления макетом страницы, открытием/закрытием панелек и загрузкой вьюшек

(function () {
    'use strict';

    angular.module('app.core').factory('shellService', shellService);

    shellService.$inject = [
        '$rootScope', 
        '$window', 
        '$location',
        '$route', 
        '$timeout', 
        'routerHelper', 
        '_storage', 
        '_tree',
        '_resource'
    ];

    function shellService($rootScope, $window,$location,$route,$timeout,routerHelper, _storage, _tree, _resource) {

        var service = {
            name: 'shellService',
            routeName: null,
            routeUrl: null,
            searchIsRunning: false,
            personShow: false,
            panels: {
                search: false,
                theme: false,
                left: false,
                right: false
            },
            leftPanelSavedState: null,  // Помогает восстановить состояни левой панели при переходе из карточки в справочник нажатием пункта меню
            savedPersonCrutch: null,    // Помогает восстановить выбранное ДЛ при переходе из б.поиска и карточки (где-то в пути он затирается)
            personGraveyardId: null,    // ID кладбища
            personReserveId: null,      // ID кадрового резерва
            themes: [
                { name: 'Цветовая схема по умолчанию', id: 'normalTheme' },
                { name: 'Затемненная цветовая схема', id: 'darkTheme' },
                { name: 'Светлая цветовая схема', id: 'lightTheme' }
            ],                        // Содержит все возможные цветовые схемы
            // Если в хранилище есть индекс темы и при этом он больше -1, то берем его, иначе ставим в ноль
            activeThemeIndex: _storage.getPerm('activeThemeIndex') ? _storage.getPerm('activeThemeIndex') > -1 ? _storage.getPerm('activeThemeIndex') : 0 : 0,
            cancelQuickSearch: cancelQuickSearch, // Отменить быстрый поиск
            getSavedPanelState: getSavedPanelState,// Достает сохраненное состояние заданной панели
            getStackColor: getStackColor,       // Возвращает цвет стека
            getRoute: getRoute,                 // Использует getRoute из routerHelper для формирования route{name,url}
            loadListView: loadListView,         // Загрузка вьюхи
            loadIndexView: loadIndexView,       // Загрузка вьюхи
            loadDataView: loadDataView,         // Загрузка вьюхи
            loadSearchView: loadSearchView,     // Загрузка вьюхи
            openOGV: openOGV,                   // Загрузка ОГВ
            orderPersonShow: orderPersonShow,   // Установить флаг открытия ДЛ из сервиса 
            panelState: panelState,             // Метод для изменения/получения статуса панелей (search,theme,left,right)
            resetTableState: resetTableState,   // Генерирует событие для сброса соответствующего объекта в localStorage
            reset: reset,                       // Обновляет состояние главного меню (использует routerHelper)
            resetPersonShow: resetPersonShow,   // Сбросить флаг открытия ДЛ из сервиса (ДЛ будет взято из хранилища)
            reloadModule: reloadModule,         // Перезагрузка текущего модуля со сбросом данных
            savePanelsState: savePanelsState,   // Сохраняет в хранилище объект panels
            setStackColor: setStackColor,       // Меняет цвет стека
            setTheme: setTheme,                 // Меняет цветовую схему
            isReserve: isReserveOrGraveyardId,  // Проверить id на совпадение с резервным или грейвярдным
            pleaseStop: false
        }

        init();
        // При попытке выбрать плитку с несуществующим ОГВ сбросить всё в ноль
        $rootScope.$on('errorSelectingChildDepartment', function () {
            reloadModule('list');
        })
        // При попытке открыть несуществующий ОГВ сбросить всё в ноль
        $rootScope.$on('errorLoadingParentDepartment', function () {
            reloadModule('list');
        })
        // Обработака сигнала интерсептора о плохом статусе полученного пакета
        $rootScope.$on('AccessError', function () {
            console.warn('Требуется авторизация!');
            var authUrl = MyApp.rootPath + 'Account/Login';
            $window.location.href = authUrl;
        });
        // Исправление пропадающих плейсхолдеров с IE8-IE9
        $rootScope.$on('fixPlaceholders', function () {
            $().placeholder && $('input, textarea').placeholder();
        });
        // Ловит сообщение от routeService и сохраняет у себя его путь и имя загруженного модуля
        $rootScope.$on('newRoute', function (event, route) {
            saveRoute(route);
        });
        // При обновлении дерева организаций нужно перезагрузить индексВью или листВью
        $rootScope.$on('newTree', function (event, ready) {

            // Если в новом окне открывается карточка ДЛ, то никаких переходов не должно быть:
            if ($route.current.params.personId) return;            
            $timeout(function () {
                if (!ready) return;
                // Отработка ситуации работы поиска или перехода в модуль Поиск
                if (service.searchIsRunning || service.routeName === 'search' || service.routeName === 'show') {
                    return;
                }
                // При попытке открыть пустое дерево должно перекинуть в коркнь
                if (_tree.elements.length === 0) {
                    service.reloadModule('list');
                    return;
                }
                service.resetTableState('list');
                !_tree.parent && service.loadIndexView();
                _tree.parent && (_tree.parent.PositionCount > 0 || _tree.parent.active !== null) ? service.loadListView() : service.loadIndexView();
            });
        });
        // При появлении выбранного узла загрузить листВью, при удалении - индексВью
        $rootScope.$on('selectionOnTree', function (event, index) {
            service.resetTableState('list');
            index === -1 && (!_tree.parent || _tree.parent.PositionCount < 1) ? service.loadIndexView() : service.loadListView();
        });
        // Если появилась выбранная персона - открыть правую панель, иначе закрыть
        $rootScope.$on('personSelected', function (event, selected) {
            var flag = selected ? true : false;
            service.panelState('right', flag);
        });
        // Перехват загрузки модулей, очень мутная проверка, тут могут вылезти баги
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            // Если открывается индекс и при этом имеется активная организация, либо родительская группа имеет
            // прикрепленных к ней должностных лиц, то нужно переключиться на listView
            try {
                if (next.$$route && next.$$route.originalPath === '/Directory'
                    && (!service.searchIsRunning // не происходит поиск
                        && _tree.active // есть выбранный элемент
                            || _tree.parent // или в родительском элементе есть пассажиры
                                && _tree.parent.PositionCount > 0)) {
                    event.preventDefault();
                    service.loadListView();
                }
            } catch (e) { error(e); }
            // При начальной загрузке данные берутся из хранилища, поэтому нужно перехватить загрузку пустого листВью
            // Нужно исключить процесс быстрого поиска!
            try {
                if (next.$$route && next.$$route.originalPath === '/Directory/list'
                    && !/ogvId/.test(service.routeUrl) // не происходит открытие нового окна с ОГВ
                        && !service.searchIsRunning // не происходит поиск
                            && !_tree.active // нет выбранного элемента
                                && (!_tree.parent // и нет родителя (начальная загрузка)
                                    || _tree.parent  // или в родительском элементе нет людей
                                        && _tree.parent.PositionCount === 0)) {
                    event.preventDefault();
                    service.loadIndexView();
                }
            } catch (e) { error(); }
            // Временная заглушка: при попытке перезагрузить Карточку - перекинуть в index - не реализовано!
            try {
                if (next.$$route && next.$$route.originalPath === '/Directory/show') {
                    //event.preventDefault();
                    //service.loadIndexView();
                }
            } catch (e) { error(); }
        });

        return service;

        /* Публичные методы */

        function orderPersonShow() { service.personShow = true }

        function resetPersonShow() { service.personShow = false }

        function isReserveOrGraveyardId(id) {
            return id == service.personGraveyardId || id == service.personReserveId;
        }

        function openOGV(govDepId) {
            service.loadListView();
            service.cancelQuickSearch(); // Отключить поиск, если он был включен, иначе левая панель останется заблокированной
            $rootScope.$emit('cleanSelection');
            $rootScope.$emit('loadTreeItem', govDepId); // Сообщение для модели tree
        }
        function cancelQuickSearch() {
            $rootScope.$broadcast('cancelQuickSearch');
        }
        function setTheme(index) {
            // Проверка параметра была произведена в контроллере, сразу меняю индекс            
            service.activeThemeIndex = _storage.setPerm('activeThemeIndex', index);
            // Создание информативного объекта для отправки 
            var theme = {
                name: service.themes[index].name,
                id: service.themes[index].id,
                index: index
            }
            $rootScope.$broadcast('theme', theme);
        }
        function getStackColor() {

            if (service.routeName === 'show') {
                return 'red';
            } else {
                return 'blue';
            }
        }
        function panelState(panel, state) {
            // Если параметр panel не соответствует ни одному из ключей объекта panels, то выйти
            if (_.isEmpty(_.pick(service.panels, panel))) {
                console.error('Нераспознаваемое значение параметра panel!');
                return false;
            }
            // Если state отсутствует, то вернуть состояние панельки и выйти
            if (typeof state === 'undefined') {
                return service.panels[panel];
            }
            // Преобразование возможного строчного статуса в true/false
            if (typeof state !== 'boolean') {
                if (state === 'open' || state === 'close') {
                    state = state === 'open' ? true : false;
                } else {
                    console.error('Нераспознаваемое значение параметра state!');
                    return false;
                }
            }
            // Основной функционал

            switch (panel) {

                case 'search':
                    if (state) {
                        if (service.panels.theme) {
                            // Если открыта панель выбора темы, то предварительно закрыть её (нужна задержка)
                            service.panels.theme = false;
                            $rootScope.$broadcast('themePanel', false);
                            setTimeout(function () {
                                $rootScope.$broadcast('searchPanel', true);
                            }, 300);
                        } else {
                            $rootScope.$broadcast('searchPanel', true);
                        }
                    } else {
                        $rootScope.$broadcast('searchPanel', false);
                    }
                    break;
                case 'theme':
                    if (state) {
                        if (service.panels.search) {
                            // Если открыта панель поиска, то предварительно закрыть её (нужна задержка)
                            service.panels.search = false;
                            $rootScope.$broadcast('searchPanel', false);
                            setTimeout(function () {
                                $rootScope.$broadcast('themePanel', true);
                            }, 300);
                        } else {
                            $rootScope.$broadcast('themePanel', true);
                        }
                    } else {
                        $rootScope.$broadcast('themePanel', false);
                    }
                    break;
                case 'left':
                    if (state) {
                        angular.element(document.querySelector('#wrapper')).removeClass('toggled');
                        // Задержка появления плиток на открывающейся панельке
                        $timeout(function () {
                            $rootScope.$broadcast('leftPanel', true);
                        }, 100);
                    } else {
                        $rootScope.$broadcast('leftPanel', false);
                        angular.element(document.querySelector('#wrapper')).addClass('toggled');
                    }
                    break;
                case 'right':
                    if (state) {
                        var dependingPanel = service.panels.search ? 'search' : service.panels.theme ? 'theme' : null;
                        if (dependingPanel) {
                            // Если открыта панель поиска или выбора темы, то предварительно закрыть их
                            service.panels[dependingPanel] = false;
                            $rootScope.$broadcast(dependingPanel + 'Panel', false);
                            setTimeout(function () {
                                $rootScope.$broadcast('personInfo', true);
                            }, 300);
                        } else {
                            $rootScope.$broadcast('personInfo', true);
                        }
                    } else {
                        $rootScope.$broadcast('personInfo', false);
                    }
                    break;
                default:
                    $log.error("Что-то пошло не так!");
                    return false;
            }
            // Сохранение нового статуса и фиксация состояния панелей в LS
            service.panels[panel] = state;
            savePanelsState();
        }
        function savePanelsState(callback) {
            // Проврка адекватности параметра
            if (!(typeof callback === 'function' || typeof callback === 'undefined')) {
                console.error('Ошибка параметра!');
                return false;
            }
            _storage.set(service.routeName, 'panels', _.clone(service.panels));
            callback && callback();
        }
        function getSavedPanelState(panel) {
            return _storage.get(shellService.routeName, 'panels')[panel];
        }
        function setStackColor(color) {
            switch (color) {
                case 'red':
                    $rootScope.$broadcast('SetStackColorRed', true);
                    break;
                case 'blue':
                    $rootScope.$broadcast('SetStackColorRed', false);
                    break;
            }
        }
        function getRoute() {
            var route = routerHelper.getRoute(); // Перенаправление
            saveRoute(route); // Сохранение
            return route;
        }
        function resetTableState(state) {
            typeof state === 'string' && $rootScope.$broadcast('resetTableState', state);
        }
        function reset(callback) {
            var config = routerHelper.config;
            var route = service.getRoute();
            var menuItem = _.findWhere(config.menu, { route: route.url });
            var parent = angular.element('#' + config.menuId).find('ul.nav.navbar-center');
            parent.find('li > a').removeClass(config.activeClass);
            parent.find('li#' + menuItem.navId).find('a').addClass(config.activeClass);
            $rootScope.$broadcast('newRoute', { 'name': route.name, 'url': route.url });
            route === 'show' ? setStackColor('red') : setStackColor('blue');
            typeof callback === 'function' && callback();
        }
        function reloadModule(name) {
            _storage.resetModule(name);
            $location.url(service.routeUrl);
            $timeout(function () {
                $window.location.reload();
            }, 100);
        }
        // Загрузка модулей:
        function loadListView() {
            service.panelState('right', 'close');
            // Необходимо перезагружать вью при переходах с плитки на плитку
            $location.$$url === '/Directory/list' ? $route.reload() : $location.url('/Directory/list');
            service.setStackColor('blue');
        }
        function loadIndexView() {
            service.panelState('right', 'close');
            //?!? $location.$$url === '/Directory' ? $route.reload()) : $location.url('/Directory');
            $location.url('/Directory');

        }
        function loadDataView() {
            service.panelState('right', 'close');
            $timeout(function () {
                $location.url('/Directory/show');
            });
        }
        function loadSearchView() {
            service.panelState('right', 'close');
            $timeout(function () {
                $location.url('/Directory/search');
            });
        }

        /* Приватные функции */

        function init() {
            if (window.addEventListener) {
                window.addEventListener('resize', resizeAlert, true);
            }
            else {
                //IE8
                window.attachEvent('onresize', resizeAlert);

            }
            _resource.getPersonReserveId(function (id) {
                if (_.isEmpty(id)) return;
                service.personReserveId = id.replace(/"/g, ""); // Убрать все кавычки
                $rootScope.$broadcast('personReserveId', id);
            });
            _resource.getPersonGraveyardId(function (id) {
                if (_.isEmpty(id)) return;
                service.personGraveyardId = id.replace(/"/g, ""); // Убрать все кавычки
                $rootScope.$broadcast('personGraveyardId', id);
            });
        }

        function resizeAlert() {
            $rootScope.$broadcast('newWindowSize', { width: angular.element(window).width(), height: angular.element(window).height() });
        }

        function saveRoute(route) {
            service.routeName = route.name;
            service.routeUrl = route.url;
        }
        function error(e) {
            console.error('Ошибка в модуле shellService!');
            e && console.log(e);
        }
    }
})();