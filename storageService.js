﻿// Сервис хранения данных, все методы возвращают либо данные, либо true/false в качстве результа
// Для временного и резервного хранилища используется sessionstorage
// Добавлен опциональный параметр suffix, позволяющий сохранять различные состояния одного модуля
(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('_storage', factory);

    factory.$inject = ['$localStorage','$sessionStorage','$cookies'];

    function factory($localStorage,$sessionStorage,$cookies) {
        var service = {
            preventRestore:false,// флаг, необходимый для предотвращения восстановления модуля при нажатии на строку таблицы в модуле List когда открыта верхняя панель
            set: set,           // поместить на временное хранение (moduleName,itemName,itemValue)
            get: get,           // считать из временного хранилища (moduleName,itemName)
            have: have,         // прверка наличия элементов в хранилище (moduleName, itemName)
            resetModule: reset, // сброс данных модуля (moduleName) во всех хранилищах
            storeModule: store, // скопировать данные модуля из временного в резервное хранилища (moduleName)
            restoreModule: restore,// восстановить данные модуля во временном хранилище из резервного (moduleName)
            stored: stored,     // проверить наличие данных модуля в резервном хранилище (moduleName)
            setPerm: setPerm,   // положить данные на постоянное хранение - в localstorage
            getPerm: getPerm,   // считать данные из постоянного хранилища - в localstorage
            clone: clone        // 
        };

        init();

        return service;

        /* ========================================================= */
        /*                           Методы                          */
        /* ========================================================= */


        function set(moduleName, itemName, itemValue) {
            if(!_.isString(itemName)) return false;
            if (_.isString(moduleName)) {
                if (!$sessionStorage[moduleName])
                    $sessionStorage[moduleName] = {}; // Первичная инициализация
                $sessionStorage[moduleName][itemName] = clone(itemValue);
            } else {
                $sessionStorage[itemName] = itemValue;
            }
            return true;
        }
        function get(moduleName, itemName) {
            if (!_.isString(itemName) || !_.isString(itemName) || !$sessionStorage[moduleName]) return false;
            return _.isString(moduleName) ? $sessionStorage[moduleName][itemName] : $sessionStorage[itemName];
        }
        function have(moduleName, itemName) {
            if (!moduleName || !moduleName) return false;
            if (!_.isString(itemName) || !_.isString(moduleName) && !_.isUndefined(moduleName)) return error('В хранилище отсутствуют данные раздела ' + moduleName);
            if (moduleName)
                return $sessionStorage[moduleName] && $sessionStorage[moduleName][itemName] ? true : false;
            else
                return $sessionStorage.itemName ? true : false;
        }
        function reset(moduleName) {
            if (_.isString(moduleName)) {
                $sessionStorage[moduleName] = null;
                $sessionStorage.neoSavedState && (function(){$sessionStorage.neoSavedState[moduleName] = null})();
                return true;
            } else return false;
        }
        function store(moduleName, callback, suffix) {
            if (!_.isString(moduleName)) return _.isFunction(callback) ? callback(false) : false;
            suffix = suffix ? suffix : '';
            try {
                $sessionStorage.neoSavedState = $sessionStorage.neoSavedState ? $sessionStorage.neoSavedState : {}; // Инициализация
                $sessionStorage.neoSavedState[moduleName+suffix] = $sessionStorage[moduleName] ? clone($sessionStorage[moduleName]) : null;
            } catch (e) { error(e); }
            return _.isFunction(callback) ? callback(true): true;
        }
        function restore(moduleName, callback, suffix) {
            if (service.preventRestore) {
                service.preventRestore = false;
                return _.isFunction(callback) ? callback(true) : true;
            }
            if (!_.isString(moduleName)) return _.isFunction(callback) ? callback(false) : false;
            suffix = suffix ? suffix: '';
            try {
                if ($sessionStorage.neoSavedState) {
                    $sessionStorage[moduleName] = $sessionStorage.neoSavedState[moduleName + suffix] ? clone($sessionStorage.neoSavedState[moduleName + suffix]) : $sessionStorage[moduleName];
                    $sessionStorage.neoSavedState[moduleName + suffix] = null; // Очистить глубокой хр-ще после изъятия объекта
                }
            } catch (e) { error(e); }
            return _.isFunction(callback) ? callback(true) : true;
        }
        function stored(moduleName, suffix) {
            if (!_.isString(moduleName)) return false;
            suffix = suffix ? suffix : '';
            try{
                return $sessionStorage.neoSavedState[moduleName+suffix] ? true : false;
            } catch (e) { error(e); }
        }
        function setPerm(itemName, itemValue) {
            if (!_.isString(itemName)) return false;
            $localStorage[itemName] = clone(itemValue);
            return itemValue;
        }
        function getPerm(itemName) {
            if (!_.isString(itemName)) return false;
            return $localStorage[itemName];
        }

        /* Приватные функции */
        function clone(obj) {
            return JSON.parse(JSON.stringify(obj));
        }
        function error(err) {
            console.warn('Ошибка обращения к локальному хранилищу!\r\n' + err);
            return false;
        }
        function init(){
            //$localStorage.neoSavedState = {};
        }
    }
})();