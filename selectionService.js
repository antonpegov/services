// Сервис для реализации выделения какого-либо человека в модулях Справочник и Расширенный поиск.
// Генерирует событие personSelected с нагрузкой true при выделении и false при снятии выделения.
// listService и searchService используется только для выкачивания из них tableData.
(function () {
    'use strict';
    angular
        .module('app.core')
        .factory('_selection', selectionService );

    selectionService.$inject = ['$rootScope', '$timeout', '$q','listService','searchService','shellService', '_storage'];

    function selectionService($rootScope, $timeout, $q, listService, searchService, shellService, _storage) {
        var service = {
            name: "selectionService",
            row: null,
            isSelected: true,       // Похоже, не используется
            isFirst: false,         // Маркеры для обозначения положения выделенной строки внутри загруженных в таблицу данных,
            isLast: false,          // обновляются в фунции init, вызываемой при каждом новом выделении, перенесены в tableData
            delayedSelection: null, // Индекс строки для отложенного выбора
            delayedPromise: null,   // Промис для реализации отложенного выбора
            sourceService: null,    // Сервис, с данными которого предстоит работать (listService или searchService)
            defScroll: null,        // Промис, скрорящий таблицу вниз после получения данных
            selected: {
                person: null,
                personIndex: null,
                module: null,
            },
            tableData: {},

            orderDelayedSelection: orderDelayedSelection,       // создает промис отложенного выбора строки
            resolveDelayedSelection: resolveDelayedSelection,   // осуществляет выбор строки в таблице персон после смены страницы 
            getDelayedPromise: getDelayedPromise,           // возвращает сохраненный промис отложенного выбора строки
            getIndex : getSelectedPersonIndex,              // возвращает индекс выбранной персоны, или -1 если её нет
            save: save,                                     // Сохраняет объект selected в сервисе _storage
            restoreSelection: restoreSelection,             // Восстанавливает объект selected из Local Storage
            resetTableData: resetTableData,
            moveUp: moveUp,
            moveDown: moveDown,
            select: select,
            unselect: unselect,
            clean: clean,
            isReady: ready                                      // Проверка отсутствия отложенного селекта
        }

        // Событие, генерируемое директивой personInfo при нажатии на иконку закрытия (дублируется в shellService)
        //$rootScope.$on('slider_personInfo_closing', function (event, page) {
        //    service.unselect().then();
        //});

        // Грубая очистка данных сервиса без каких-либо дополнительных действий
        $rootScope.$on('cleanSelection', function () {
            service.delayedSelection = null;
            service.delayedPromise = null;
            service.selected = {
                person: null,
                personIndex: null,
                module: null
            }
        });

        // Сбрасывает выбранную персону при смене страницы таблицы
        $rootScope.$on('st-page-changed', function (event, page) {
            var oldPage = service.tableData.currentPage;
            service.defScroll = $q.defer();
            // Если есть выбранный человек и идет переход на следующую(+1) страницу, то нужно скроллить вверх!
            service.defScroll.promise.then(function () {
                $timeout(function () {
                    if (service.delayedPromise && page-oldPage === 1) {
                        angular.element('.tablecontainer').scrollTop(0);
                        angular.element('.advanced-search-tables').scrollTop(0);
                    } else {
                        angular.element('.tablecontainer').scrollTop(10000);
                        angular.element('.advanced-search-tables').scrollTop(10000);
                    }
                });
            });
            service.unselect();
        });
            // После загрузки нового списка людей нужно проверить, нет ли заказанного отложенного выбора
        $rootScope.$on('personsLoaded', function (event, loaded) {
            if (loaded === false) return; // Данные еще не загрузились
            $rootScope.$broadcast('stBlockPagination', false); // Восстанавливает чувствительность пейджинга  
            try {
                getSourceService(function () {
                    service.tableData = service.sourceService.tableData;
                    status();
                    service.delayedPromise // Если присутствует заказ на отложенный выбор
                        && true// И если есть объект с соответствующим Id
                            && service.resolveDelayedSelection(); // Запустить отложенный выбор
                });
            } catch (e) {
            //console.warn('Сбой при подключении tableData сервиса %s.', service.sourceService ? service.sourceService.name : '<пусто>');

            }
            if (loaded && service.defScroll) {
                service.defScroll.resolve();
                service.defScroll = null;
        }
        });
            // Загрузился новый модуль
        $rootScope.$on('newRoute', function () {
            //init(); Убрал, т.к. вызывать выбор до загрузки данных бессмысленно
        });

        init();

        return service;

            /* ========================================================= */
            /*                           Методы                          */
            /* ========================================================= */

        function ready(moduleName) {
                return service.delayedPromise === null || moduleName 
                            && service.delayedPromise 
                            && service.delayedPromise.promice 
                            && service.delayedPromise.promice.module !== moduleName ? true : false;
        }
            function getDelayedPromise() {
            if (service.delayedPromise) {
                return service.delayedPromise.promise;
            } else {
                console.error('Ошибка создания промиса отложенного выбора строки!');
                // Создаю фейковый промис для предотвращения падения приложения в случае проблемм с оригиналом
                var def = $q.defer();
                return def.promise;

            }
        }
            function orderDelayedSelection(index, name) {
                // Проверка наличия индекса в вызове
            if (typeof index !== 'number') {
                // Проверка наличия индекса в сервисе 
                if (service.selected && typeof service.selected.personIndex === 'number') {
                    index = service.selected.personIndex > - 1 ? service.selected.personIndex : -1;
                } else {
                    console.error('Ошибка при попытке заказать отложенный выбор - нет индекса для выбора!');
            }
            }
            service.delayedPromise = $q.defer();
            service.delayedPromise.promise.name = name ? name : null;
            service.delayedSelection = index;
        }
            function resolveDelayedSelection() {

            if (typeof service.delayedSelection !== 'number') {
                //$log.error('delayedSelection is not a number!');
                return false;
            } else if (service.delayedPromise === null) {
                $log.error('delayedPromice is NULL!');
                return false;
            } else {
                service.select(service.delayedSelection).then(function () {
                    service.delayedPromise.resolve(true);
                    service.delayedPromise = null;

                });
            }
        }
            function moveUp() {
                // Если выбрана первая строка не первой страницы, то нужно организовать выбор строки 
                // после перезагрузки таблицы, для чего использую создание "промиса отложенного выбора"
            if (service.tableData.isFirst) {
                if (service.tableData.options.page < 2) {
                    console.error('Это самая первая запись!');
                } else {
                    // Заказ отложенного выбора строки
                    service.orderDelayedSelection(service.tableData.options.pageSize -1, 'moveUp');
                    // Переход на предыдущую страницу
                    $rootScope.$broadcast('stSelectPage', service.tableData.options.page -1)
                    
                    return unselect(getSelectedPersonIndex())
                        .then(function (index) {
                            // Возвращаю отложенный промис для его последующей обработки
                            return service.getDelayedPromise();
                    });
            }
            } else {
                // Определить расстояние доо верхней границы контейнера и при необходимости поскролить вверх
                //TODO             
                // Очистить выбор и выбрать предыдущую строку
                return unselect(getSelectedPersonIndex())
                    .then(function (index) {
                        return select(index -1);
                })
                    .then(function (index) {
                        init(index);
                })
                    ["catch"](function (msg) {
                        console.error(msg);
                });

            }
        }
            function moveDown() {
                // Если выбрана первая строка не первой страницы, то нужно организовать выбор строки 
                // после перезагрузки таблицы, для чего использую создание "промиса отложенного выбора"
            if (service.tableData.isLast) {
                if (service.tableData.isLastPage) {
                    console.error('Это самая последняя запись!');
                } else {
                    $rootScope.$broadcast('stSelectPage', service.tableData.options.page +1);
                    // Заказ отложенного выбора строки
                    service.orderDelayedSelection(0, 'moveDown');
                   
                    return unselect(getSelectedPersonIndex())
                        .then(function (index) {
                            // Возвращаю отложенный промис для его последующей обработки
                            return service.getDelayedPromise();
                    });
            }
            } else {
                // Определить расстояние доо верхней границы контейнера и при необходимости поскролить вниз
                //TODO 
                // Очистить выбор и выбрать следующую строку
                return unselect(getSelectedPersonIndex())
                    .then(function (index) {
                        return select(index +1);
                })
                    .then(function (index) {
                        init(index);
                })
                    ["catch"](function (msg) {
                        console.error(msg);
                });
            }
        }
        function select(index, now) {
            var def = $q.defer();
            def.promise.name = 'select';
            if (index > -1) {
                clean().then(function () {
                    // Костыль для обхода глюка smartTable, который вторым вызовом pipe (из stPersist) затирает выбранного человека в гриде
                    if (typeof service.delayedSelection === 'number') {
                        $timeout(function () {
                            delayedSelection(def);
                        }, 500);
                    } else {
                        delayedSelection(def);
                    }
                    service.delayedSelection = null;
                    //$rootScope.$broadcast('checkIfSelectionIsNotFallOut');
                });
            }
            init(index);

            return def.promise;

                function delayedSelection(def) {
                try {
                    var person = service.tableData.persons[index];
                    person.isSelected = true;
                    service.selected.person = person;
                    service.selected.personIndex = index; //_.indexOf(service.tableData.persons, person);
                    service.selected.module = shellService.routeName;
                    //status();
                    service.save();
                    $rootScope.$broadcast('personSelected', service.selected);
                    def.resolve(index);
                } catch (e) {
                    console.warn('Не возможно восстановить выбор должностного лица (index=%d):', index);
                    service.selected = { person: null, personIndex: null, module: null
                };
                    $rootScope.$broadcast('personSelectionError');
                    def.reject();
                }

            }
        }
        function unselect(index) {
            var def = $q.defer();
            index = index ? index : getSelectedPersonIndex();
            if (typeof index !== 'number' || index === -1) {
                def.resolve(-1);
                return def.promise;
            }
            def.promise.name = 'unselect';
            service.clean(index).then(function () {
                def.resolve(index);
                service.save();
                $rootScope.$broadcast('personSelected', false);
                //status();
            });
            return def.promise;
        }
        function restoreSelection(moduleName, callback) {
            moduleName = moduleName ? moduleName : shellService.routeName;
                // Берем предварительно восстановленный объект из хранилища, если он там есть
            service.selected = _storage.have(moduleName, 'selected') ? _storage.get(moduleName, 'selected') : service.selected;
            _.isFunction(callback) && callback();
                // При отсутствии колбека самому попытаться запустить выделение,
                // если в таблице есть персона с соответствующим Id
            !callback
                && service.selected.person
                    && (service.selected.module === moduleName)
                        && service.tableData
                            && _.find(service.tableData.persons, function (item) {
                                return item.Id === service.selected.person.Id;
            })
                                && service.select(service.selected.personIndex);
        }

        /* Cлужебные функции */

        // Вычищает свойство isSelected у элементов массива загруженных персон
        function clean(index) {
            var def = $q.defer();
            def.promise.name = 'clean';
            if (typeof index === 'number' && index > -1) {
                service.tableData.persons[index].isSelected = false;
            } else {
                for (var i = 0; i < service.tableData.persons.length; i += 1) {
                    service.tableData.persons[i].isSelected = false;
            }
            }
            service.selected.person = null;
            service.selected.personIndex = null;
            service.selected.module = null;
            _storage.set(shellService.routeName, 'selected', service.selected);
            def.resolve(true);

            return def.promise;
        } // перестала работать, пришлось подставлять костыли
        function getSelectedPersonIndex() {
            if (service.tableData && service.tableData.persons && service.selected.person) {
                for (var i = 0; i < service.tableData.persons.length; i += 1) {
                    if (service.tableData.persons[i].Id === service.selected.person.Id && i === service.selected.personIndex)
                        return i;
            }
            }
            return -1;
        }
        function save(selected) {
            var selected = angular.isUndefined(selected) ? service.selected : selected; // Если параметр пустой, взять из сервиса
            _storage.set(shellService.routeName, 'selected', selected);
        }
        function getSourceService(callback) {
            service.sourceService =
                shellService.getRoute().name === 'list' ? listService :
                    shellService.getRoute().name === 'search' ? searchService :
                            null;
            _.isFunction(callback) && callback();
        }
        function status() {
            $rootScope.$broadcast('selectionReady', true);
        } // Информирует окружающих о занятости(false)/готовности(ready) сервиса
        function resetTableData() {
            service.tableData = {};
        }
        function init(index) {

                // Перезапуск селекта после перезагрузки страницы
            _.isUndefined(index)
                && _.isEmpty(service.selected.person)
                    && _storage.have(shellService.getRoute().name, 'selected') // getRoute() инициализирует routeName
                        && _storage.get(shellService.routeName, 'selected')
                            && restoreSelection();
                // Выбираем сервис, из которого брать tableData
            getSourceService(function () {
                service.tableData = service.sourceService ? service.sourceService.tableData : null;
                if (typeof index === 'number' && index > -1 && service.sourceService && service.sourceService.tableData) {
                    service.tableData.isFirst = index === 0 ? true : false;
                    if (!service.tableData.isLastPage) {
                        service.tableData.isLast = index === service.tableData.options.pageSize - 1 ? true : false;
                    } else {
                        service.tableData.isLast = index === service.tableData.persons.length - 1 ? true : false;
                }
            }
            });

                //if (!listService.tableData.isLastPage) {
                //    service.isLast = index === listService.tableData.options.pageSize - 1 ? true : false;
                //} else {
                //    service.isLast = index === listService.persons.length-1 ? true : false;
                //}
        }
    }
})();
