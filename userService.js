﻿(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('userService', userService);

    userService.$inject = [];
    function userService() {
        if (typeof AppUser === 'undefined') {
            console.error('Пользователь не идентифицирован!');
            return false;
        }
        //1,2,3,4 - ничегo, идентификационные данные, плюс перс.данные, все   
        var roles = {
            Administrator: { access: 5, role: 'aдминистратор' },
            Analytic: { access: 4, role: 'аналитик' } ,
            Operator: { access: 3, role: 'оператор' } ,
            User: { access: 2, role: 'пользователь' } ,
            Unknown: { access: 1, role: 'гость' } ,
            Wtf: {access: 0, role: 'глюк'}
        };
        var service = {
            name: AppUser.Name ? AppUser.Name : 'Guest',
            role: AppUser.Role ? roles[AppUser.Role].role : 'гость',
            access: AppUser.Role? roles[AppUser.Role].access : 1          
        };

        init();
        return service;

        function init() {
            if (service.access >= 2) {
                console.log('Добро пожаловать, %s %s!', service.role, service.name);
            } else {
                console.error('Сначала авторизуйтесь, уважаемый!');

            }
        }
    }
})();