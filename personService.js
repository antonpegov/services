﻿// При получении списка людей бродкастит событие personsLoaded (см. метод pipe)

(function () {
	'use strict';

	angular
		.module('app.core')
		.factory('listService', listService);

	listService.$inject = [
        '$rootScope',
        '$q',
        '$localStorage',
        'apiHelper',
        'shellService',
        '_tree',
        '_resource'];

	function listService($rootScope,$q,$localStorage,apiHelper,shellService,_tree,_resource) {

		var service = {
			name: 'listService',
			persons: [],                    // Массив для хранения актуального списка госслужащих
			activeGovernmentDepartment:null,// Организация, работники которой загружены 
			searchMode: 1,                  // 0 - по организациям, 1 - по людям
			searchTerm: null,               // строка поиска
			tableData: {
				//coldStart: true,                       // Триггер первого запуска pipe
				tableStateName: 'personTableState',    // Имя свойства в local storage
				pageSizes: [10, 15, 20, 50, 100],
				itemsByPage: 50,
				total: 0,
				isLastPage: false,
				options: {},
				tableState: {},
				currentPage: 1,
				pipe: pipe

			},

            openOgvById: openOgvById,       // Инициирует загрузку произвольного ОГВ
			cleanDate : cleanDate,          // Очищает дату от лишнего телериковского г.
			pipe: pipe,                     // Загружает данные для таблицы
		};
		
		init(); // Подготовка к запуску, очистка лишних данных, подтянутых из хранилища

		return service;
		
		function init() {
			// Если при старте отсутствует выбранная группа, то нужно очистить стек и сбросить группу в плиточной директиве
			if (!service.parentId) {
				service.govDepStack = [];
				//$rootScope.$broadcast('newGovDepReady');
			}
		}
		function pipe(tableState) {
		    
			var tData = service.tableData;
			// При "холодном старте" нужно взять tableState из local storage (см. директиву st-persist)
			//if (tData.coldStart && !service.searchTerm) {
			//	tData.coldStart = false;
			//}
            
            // Загрузить из дерева ОГВ либо выбранный элемент, либо, если он отсутствует, родительский элемент:
			service.activeGovernmentDepartment = _tree.active ? _tree.active : (_tree.parent && _tree.parent.ChildCount > 0) ? _tree.parent : null;
			// Сбор объекта конфигурации запроса
			var options = {};
			options.pageSize = tData.itemsByPage;
			options.page = tableState.pagination.start < tableState.pagination.number ? 1 : (tableState.pagination.start / tableState.pagination.number) + 1;
			options.sort = (tableState && tableState.sort.predicate) ? tableState.sort.predicate + '-' + (tableState.sort.reverse ? 'asc' : 'desc') : service.searchTerm ? '' : 'Order-asc';
			options.searchTerm = service.searchMode === 1 ? service.searchTerm : null;
			options.group = null;
		    // При восстановлении данные берутся из Local Storage
			options.govDepId = service.activeGovernmentDepartment ? service.activeGovernmentDepartment.Id : null; // Id организации, чьих сотрудников нужно показать
			tData.tableState = tableState;  // Сохранения состояния таблицы в сервисе
			tData.options = options;        // Сохранение параметров запроса
			shellService.panelState('right', 'close'); // Закрыть правую панель	
		    // Если происходит поиск по людям, то предварительно очистить govDepId и activeGovernmentDepartment
			if (service.searchMode === 1 && _.isString(service.searchTerm) && service.searchTerm.length > 0) {
			    options.govDepId = service.activeGovernmentDepartment = null; 
			} 
			$rootScope.$broadcast('personsLoaded', false); // Сигнализировать о начале процесса загрузки
			_resource.getPersons(options.govDepId, options, function (data) {
			    var persons = _.isObject(data.Data) ? data.Data : [];
			    //Сохраняем полученные данные в сервисе:
			    service.tableData.persons = persons;
			    service.tableData.total = data.Total;
			    service.tableData.currentPage = options.page;
			    service.tableData.totalPages = service.tableData.total / options.pageSize;
			    // Если кол-во страниц получилось с остатком - отбросить дробную часть и добавить одну страницу
			    if (service.tableData.totalPages % 1 > 0) {
			        service.tableData.totalPages = service.tableData.totalPages - (service.tableData.totalPages % 1);
			        service.tableData.totalPages += 1;
			    }
			    service.tableData.isLastPage = service.tableData.totalPages === service.tableData.currentPage ? true : false;
				tableState.pagination.numberOfPages = tData.totalPages;
				$rootScope.$broadcast('personsLoaded', true); // Сигнализировать об окончании процесса загрузки
			});		    
		}
		function cleanTableState() {
			service.tableData.tableState = null;
			localStorage.setItem('tableState', null);
			$localStorage.tableState = null;
		}
		function openOgvById(ogvId) {
		    // Получить объект ОГВ по полученному Id
		        if (!_.isEmpty(ogvId)) {
		            // Оповестить сервис дерева ОГВ о необходимости перестроить дерево
		            $rootScope.$emit('loadTreeItem', ogvId);
		        } else {
		            throw new Error("Отсутствует идентификатор ОГВ!");
		        }
		}
		function cleanDate(string) {
		    if (string) {
		        if (~string.indexOf('Date')) {
		            var re = /-?\d+/;
		            var date = string.match(re);
		            //console.log(date);
		            return date[0];
		        } else {
		            return string;
		        }
		    }
		}
	};
})();
