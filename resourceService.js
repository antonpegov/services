﻿// Необходимые общие ресурсы для хранения: стек групп ОГВ

(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('_resource', factory);

    factory.$inject = ['$q', '$rootScope', 'apiHelper' ];

    function factory($q, $rootScope, apiHelper) {

        
        var abort = {
            getPersons: null,
        } /* Отменяющие промисы для публичных методов */        
        var service = {
            getPersons: getPersons,     // Загрузка массива госслужащих для указанной организации
            getJobs: getJobs,           // Загрузка массива должностей для указанного госсслужащего
            getPhotos: getPhotos,       // Загрузка фотографий
            getStack: getStack,         // Загрузка стека родительских элементов
            getOgvById: getOgvById,     // Загрузка произвольного ОГВ 
            getPersonById: getPersonById,           // Получение персоны
            getPersonReserveId: getPersonReserveId, // Получить Id кадрового резерва (виртуальный ОГВ)
            getPersonGraveyardId: getPersonGraveyardId, // Получить Id кладбища (виртуальный ОГВ)
            report: sendReport,         // Отправка замечания об ошибочных данных

            /* Блок работы с API индикаторов */
            getIndicatorsForPerson: getIndicatorsForPerson,
            getIndicatorsTree: getIndicatorsTree,
            getIndicatorControllingValue: getIndicatorControllingValue,
            getIndicatorValues: getIndicatorValues,
            getIndicatorDetails: getIndicatorDetails,
            removeIndicatorForPerson: removeIndicatorForPerson,
            setIndicatorControllingValue: setIndicatorControllingValue,
            saveIndicatorForPerson: saveIndicatorForPerson,
        }; /* API */

        return service;

        /* методы */
        
        function getPersons(govDepId, options, callback) {
            var me = 'getPersons';
            // Если параметры для формирования запроса не соответствуют ожидаемым, то отменить запрос
            if (_.isObject(govDepId || typeof options !== 'object' || options === null)) {
                console.error('Несоответствие параметров вызова!');
                return false;
            }
            if (govDepId === null) {
// TODO: С этой строкой нужно что-то делать, после переноса кода она не работает, могло что-то слететь
                govDepId = service.activeGovernmentDepartment ? service.activeGovernmentDepartment.Id : null;
                if (!govDepId && _.isEmpty(options.searchTerm)) return true;
            } // Если нет выбранного ОГВ и при этом не осуществляется поиск, то отменить запрос
            abort[me] && abort[me].resolve(); // Отменить предыдущий запрос
            abort[me] = $q.defer(); // Зарегистрировать новый отменяющий промис 
            apiHelper.apiPost('api/personposition/', options, function (result) {
                abort[me] = null; // Очистить отменяющий промис после получения результата
                if (typeof result.data === 'object') {
                    callback(result.data);                    
                } else {
                    callback(errorResp());
                }
            }, null, null, abort[me].promise); // отменяющий промис пришлось передавать дополнительным параметром
        }

        function getPhotos(id, callback) {
            if (id) {
                apiHelper.apiGet('api/PersonPhotos/' + id, null, function (result) {
                    //result.data - массив с id фотографий, проверяю не пустой ли он
                    var message = false; // Нагрузка к событию
                    if (typeof result.data !== 'object' || typeof result.data.length === 'undefined') {
                        console.error('Ошибка при получении массива фотографий с сервера!');
                        // Создаю фейковый результат для предотвращения падения приложения
                        result.data = [];
                        $rootScope.$broadcast('photosLoaded', false);
                        callback(result.data);
                        return false;
                    }
                    if (result.data.length > 0) {
                        message = true;
                    }
                    $rootScope.$broadcast('photosLoaded', message);
                    callback(result.data);
                });
            } else {
                console.error('Нет Id для загрузки фотографий');
                callback(null);
            }
        }

        function getJobs(id, callback) {
            if (!_.isString(id) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            apiHelper.apiGet('apiex/person/position/'+ id, null, function (result) {
                _.isObject(result.data) ? callback(result.data) : callback(errorResp());
            });
        }

        function getOgvById(id, callback) {
            if (!_.isString(id) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            var uri = 'apiex/govdepartment/dep/' + id;
			apiHelper.apiGet(uri, null, function (result) {
			    var govDep = result.data;
			    _.isObject(govDep) ? callback(govDep) : (function () { $rootScope.$broadcast('errorLoadingParentDepartment'); callback(errorResp()) })();
			});
        }

        function getPersonById(id, callback) {
            if (!_.isString(id) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            var uri = 'apiex/person/person/' + id;
            apiHelper.apiGet(uri, null, function (result) {
                var person = result.data;
                _.isObject(person) ? callback(person) : callback(errorResp());
            });
        }

        function getStack(govDep, callback) {
            if (!_.isFunction(callback) || _.isEmpty(govDep)) {
                console.error('Ошибочные параметры!');
                return false;
            }
            var uri = 'apiex/govdepartment/getparents/' + govDep.Id;
            var params = {};
            //canceller && canceller.resolve(); // Отменить предыдущий запрос 
            //canceller = $q.defer(); // Создать новый отменяющий промис
            //params = { timeout: canceller.promise }; // Вложить отмняющий промис в параметры запроса
            apiHelper.apiGet(uri, params, function (result) {
                var stack = result.data;
                if (typeof stack === 'object') {
                    stack = stack.reverse();
                    stack.push(govDep);
                    callback(stack);
                } else {
                    console.error("Не могу получить список департаментов от сервера!");
                }
            });
        }

        function sendReport(data) {
            var def = $q.defer();
            var uri = 'Proposal/Create';
            apiHelper.apiPost(uri, data, function (result) {
                result = typeof result.data === 'object' && typeof result.data.Id === 'string' ? true : false;
                def.resolve(result);
            }, function (error) {
                def.resolve(false);
            });
            return def.promise;
        }

        function getIndicatorsForPerson(id, callback) {
            if (!_.isString(id) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            apiHelper.apiGet('apiex/fondsettings/person/' + id, null, function (result) {
                _.isObject(result.data) ? callback(result.data) : callback(errorResp());
            })
        }
       
        function saveIndicatorForPerson(data, callback) {
            if (_.isEmpty(data) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            apiHelper.apiPost('apiex/fondsettings/add', data, function (result) {
                result.status == 200 ? callback(true, result.data) : callback(false);
            });
        }

        function removeIndicatorForPerson(id, callback) {
            if (_.isEmpty(id) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            apiHelper.apiPost('apiex/fondsettings/remove/' + id, null, function (result) {
                result.status == 200 ? callback(true) : callback(false);
            });
        }

        function getIndicatorValues(ind, callback) {
            if (_.isEmpty(ind) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            apiHelper.apiGet('apiex/fond/values/' + ind.Id, null,
                function (result) {
                    // Успешный колбек
                    result.status == 200 ? callback(true, result.data) : callback(false);
                },
                function (result) {
                    // Неуспешный колбек
                    console.error('Ошибка при запросе к серверу!');
                    callback(false);
            });
        }

        function getIndicatorsTree(callback) {
            apiHelper.apiGet('apiex/fond/indicators', null, function (result) {
                _.isObject(result.data) ? callback(result.data) : callback(errorResp());
            });
        }

        function getIndicatorDetails(indicator, callback) {
            if (_.isEmpty(indicator) || !_.isFunction(callback)) {
                errorParam();
                return false;
            } // Проверка параметров
            apiHelper.apiGet('apiex/fond/details/' + indicator.IndicatorId, null, function (result) {
                _.isObject(result.data) ? callback(result.data, indicator) : callback(errorResp());
            })
        }

        function getIndicatorControllingValue(indicator, callback) {
            apiHelper.apiGet('apiex/fond/controllingvalue?id=' + indicator.IndicatorId+'&rn='+ indicator.RN + '&period='+indicator.Period, null,
                function (result) { // Успешный колбек                    
                    result.status == 200 ? callback(true, result.data) : callback(false);
                },
                function (result) { // Неуспешный колбек
                    console.error('Ошибка при получении контрольного значения!');
                    callback(false);
            })
        }

        function setIndicatorControllingValue(ind, callback) {
            apiHelper.apiGet('apiex/fond/savecontrollingvalue?id='+ind.IndicatorId+'&rn='+ind.RN+'&period='+ind.Period+'&value='+ind._controllingValue,null,
            //apiHelper.apiGet('apiex/fond/savecontrollingvalue', {id:ind.IndicatorId, rn:ind.RN,period:ind.Period,value:ind._controllingValue}, 
                function (result) {                     
                    result.status == 200 ? callback(true, result.data) : callback(false);
                }, // Успешный колбек
                function (result) { 
                    console.error('Ошибка при получении контрольного значения!');
                    callback(false);
                }) // Неуспешный колбек
        }

        function getPersonReserveId(callback) {
            var src = 'apiex/govdepartment/getreservedid';
            apiHelper.apiGet(src, null, function (result) {
                _.isEmpty(result.data) ? callback(errorResp()) : callback(result.data) ;
            });
        }

        function getPersonGraveyardId(callback) {
            var src = 'apiex/govdepartment/getgraveyardid';
            apiHelper.apiGet(src, null, function (result) {
                _.isEmpty(result.data) ? callback(errorResp()) : callback(result.data);
            });
        }

        /* функции */

        function errorParam() {
            console.error('Ошибочный параметр!');
        }

        function errorResp() {
            console.error("Не могу получить данные от сервера!");
            return []; // Заглушка - при любой ошибке будет возвращаться пустой массив
        }

        function error(result) {
            console.error('Ошибка при запросе к серверу!');
            callback(false);
        }
    }
})();