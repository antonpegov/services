﻿// Значения критериев поиска не могут быть числами 

(function () {
    'use strict';

    angular.module('app.core').factory('searchService', searchService);

    searchService.$inject = [
        '$rootScope',
        '$q',
        '$localStorage',
        'apiHelper',
        'userService',
        '$cookies',
        '$timeout',
        '_storage'
    ];

    function searchService($rootScope, $q, $localStorage, apiHelper, userService, $cookies, $timeout, _storage) {

        
        var canceller = null;               // Отменяющий промис
        
        var service = {
            name: 'searchService',
            viewState: {},
            options: null,
            request: [],
            savedRequest: null,
            promises: [],
            defs: [],
            tableData: {
                pageSizes: [10, 15, 20, 50, 100],
                itemsByPage: 50,
                total: 0,
                isLastPage: false,
                options: {},
                tableState: {},
                currentPage: 1,
                totalPages: 0
            },

            restoreRequest: restoreRequest,
            sendSearchRequest: sendSearchRequest,
            getOptions: getOptions,
            download: download,
            downloadExp: downloadExp,
            pipe: pipe,                     
            stopRequest: stopSearchRequest  // Отменить активный запрос
        }
        
        init();

        return service;

        /* Методы */

        function pipe(tableState) {

            var tData = service.tableData;  // Состояние таблицы хранится в сервисе
            var options = {};               // Параметры пейджинга
            var sort = {};            // Сортировка по умолчанию
            tData.tableState = tableState;  // Сохранение состояния таблицы в сервисе

            if (_.isEmpty(tableState)) return false;
            if (service.request.length === 0 && !service.savedRequest) {
                tableState.pagination.numberOfPages = 0;
                return true;
            }  // При первичной загрузке pipe не нужен  
            if (tableState.sort.predicate) {
                sort[tableState.sort.predicate] = tableState.sort.reverse ? 0 : 1;
            } 
            options.sort = sort;
            tData.options = options;                                        // Сохранение параметров запроса
            $rootScope.$broadcast('personsLoaded', false);                  // Запуск блокировок/спиннеров
            if (service.request.length > 0 && service.viewState.active) {
                // Новый запрос при актуальном состоянии параметров (производится нажатием кнопки "Найти")
                options.pageSize = tData.itemsByPage;
                options.page = tableState.pagination.start < tableState.pagination.number ? 1 : (tableState.pagination.start / tableState.pagination.number)+1;
                sendSearchRequest(service.request, options)
                    .then(function (data) {
                        tData.dataReceived = true;
                        tData.persons = data.Results; // Сохранение массива полученных персон 
                        tableState.pagination.numberOfPages = tData.totalPages;
                        $rootScope.$broadcast('personsLoaded', true);
                    })
                            ["catch"](function (message) {
                                service.request = null;
                                tData.dataReceived = false;
                                tData.errorMsg = message ? message : "Ошибка сервера!";
                                $rootScope.$broadcast('personsLoadingError');
                            });
            } else if (service.savedRequest) {
                // Запрос, производимый при инициализации вьюшки, повторяет последний запрос полностью,
                // а если это перемещение по таблице при неактуальном состоянии поисковых параметров, 
                // то нужно испльзовать получаемые параметры пейджинга и сохраненный объект запроса
                options.pageSize = service.savedRequest.PageSize;
                options.page = service.savedRequest.Page;
                if (tableState.pagination.numberOfPages || tableState.pagination.numberOfPages > 1) {
                    // Если есть numberOfPages и он больше единицы, значит запрос пришел из таблицы, а не от загрузчика
                    options.pageSize = service.savedRequest.PageSize = tData.itemsByPage;
                    options.page = service.savedRequest.Page = tableState.pagination.start < tableState.pagination.number ? 1 : (tableState.pagination.start / tableState.pagination.number) + 1;
                }
                sendSearchRequest(null, options, service.savedRequest)
                    .then(function (data) {     
                        tData.dataReceived = true;
                        tData.persons = data.Results; // Сохранение массива полученных персон 
                        tableState.pagination.numberOfPages = tData.totalPages;
                        tableState.pagination.start = options.pageSize * (options.page - 1);
                        $rootScope.$broadcast('personsLoaded', true);
                    }
                )};

        }

        /**
        * Загружает с сервера условия, поля и операторы. Возвращает составной промис, которой 
        * резолвится после резолва всех его компонентов
        * @method getOptions
        * @returns {conditions: conditions.promise, fields: fields.promise, allOperators: allOperators.promise}  
        */
        function getOptions() {

            //var operators = $q.defer();
            var conditions = $q.defer();
            var fields = $q.defer();
            var allOperators = $q.defer();
            var allSelectorValues = $q.defer();

            apiHelper.apiGet('api/SearchCondition', null, function (result) {
                conditions.resolve(result.data);
            });
            apiHelper.apiGet('api/SearchField', null, function (result) {
                fields.resolve(result.data);
            });
            fields.promise.then(function (res) {
                formAllOperatorsArray(res).then(function (operators) {
                    allOperators.resolve(operators);
                });
            });
            fields.promise.then(function(res) {
                formAllSelectorValuesArray(res).then(function(selectorValues) {
                    allSelectorValues.resolve(selectorValues);
                });
            });

            var all = $q.all({ conditions: conditions.promise, fields: fields.promise, allOperators: allOperators.promise, allSelectorValues: allSelectorValues.promise });
            all.then(function (res) {
                service.options = res;
                $rootScope.$broadcast('searchOptionsReady');
            });
            return all;
        }
      
        /**
        * Производит запрос поискового результата
        * @method sendSearchRequest
        * @param grid - массив настроенных критериев поиска 
        * @returns {}  
        */
        function sendSearchRequest(grid, options, saved) {

            var def = $q.defer(); // Примис для ограничения времени ответа сервера
            var gridCopy = grid ? JSON.parse(JSON.stringify(grid)) : null; //глубокая копия с преобразованием всего в строки
            var payload = grid ? new Request(gridCopy, options) : saved ? saved : null;

            if(!payload) throw "Потерялся объект запроса!";
            canceller && canceller.resolve(); // Отменить предыдущий запрос
            canceller = $q.defer(); // Зарегистрировать новый отменяющий промис 
            apiHelper.apiPost('api/PersonSearch', payload, function (result, request) {
                if (result.status === 204) {
                    service.request = null;
                    service.tableData.dataReceived = false;
                    service.tableData.errorMsg = "Заполните минимум один критерий поиска!";
                    $rootScope.$broadcast('personsLoadingError');
                    def.reject("Заполните минимум один критерий поиска!");
                    return;
                }
                service.tableData.total = result.data.TotalCount;
                service.tableData.currentPage = options.page;
                service.tableData.totalPages = service.tableData.total / options.pageSize;
                if (service.tableData.totalPages % 1 > 0) {
                    service.tableData.totalPages = service.tableData.totalPages - (service.tableData.totalPages % 1);
                    service.tableData.totalPages += 1;
                }
                service.tableData.isLastPage = service.tableData.totalPages === service.tableData.currentPage ? true : false;
                def.resolve(injectLinks(result.data));
            }, null, null, canceller.promise);
            // Если сервер долго не отвечает, реджектим промис, чтобы не подвешивать страницу
            setTimeout(function () {
                def.reject("Превышено установленное время ожидания ответа сервера (3 мин.)");
            }, 180000);
            return def.promise;
        }

        function stopSearchRequest() {
            canceller && canceller.resolve(); // Отменить активный запрос
        }

        /* Функции */

        /**
        * Добавляет к полученному списку ДЛ маркеры для последующей отрисовки связей
        * @function 
        * @param {Array} persons 
        * @returns {}  
        */
        function injectLinks(persons) {
            if (!persons.TotalCount || persons.TotalCount < 1) return persons;
            // Перечисление типов связанности записей в таблице ДЛ
            var arr = persons.Results;
            var LinkType = { 
                NO: 'no',
                FIRST: 'first',
                MEDIUM: 'medium',
                LAST: 'last'
            }
            arr[0]._class = LinkType.NO; // первый элемент инициализируется NO
            for (var i = 1; i < arr.length; i += 1) {
                var previous = arr[i - 1];
                var current = arr[i];
                if (previous.Id == current.Id) {
                    current._class = LinkType.MEDIUM;
                    previous._class = previous._class == LinkType.NO ? LinkType.FIRST : LinkType.MEDIUM;
                } else {
                    current._class = LinkType.NO;
                    previous._class = previous._class == LinkType.NO ? LinkType.NO : LinkType.LAST;
                }
            }
            if (arr[arr.length - 1]._class == LinkType.MEDIUM) arr[arr.length - 1]._class = LinkType.LAST;
            return persons;
        }

        /**
        * Формирует массив всех возможных операторов перебирая имеющиеся DataType
        * @function 
        * @param {Array} fields - массив полей, содержащих значение DataType 
        * @returns {}  
        */
        function formAllOperatorsArray(fields) {

            if (typeof fields !== 'object' || fields === null) {
                console.error('Ошибка в функции formAllOperatorsArray(): не получен объект!');
                return false;
            }
            var i;
            var codes = []; 
            var defs = [];
            var promises = [];
            // Формирование массива кодов для последующего запроса соответствующих им списков операторов
            for (i = 0; i < fields.length; i += 1) {
                if (codes.indexOf(fields[i].DataType) === -1) {
                    codes.push(fields[i].DataType);
                }
            }
            // Запрос списков операторов и формирование из них массива
            for (i = 0; i < codes.length; i += 1) {
                var code = parseInt(codes[i]);
                defs[code] = $q.defer();
                promises.push(defs[code].promise);
                apiHelper.apiGet('api/SearchOperator/' + code, null, function (result) {
                    defs[result.data.DataType].resolve({
                        DataType: result.data.DataType,
                        operators: result.data.Data
                    });
                });
            }
            return $q.all(promises);
        }

        /**
                * Формирует массив всех возможных SelectorValues перебирая имеющиеся SelectorType
                * @method formSelectorValuesArray
                * @param {Array} fields - тип селекттора, полученный из SearchFields 
                * @returns {Promise.<Array>} - массив из списков SelectorValeue  
                */
        function formAllSelectorValuesArray(fields) {
                
            if (typeof fields !== 'object' || fields === null) {
                console.error('Ошибка в функции formAllSelectorValuesArray(): не получен объект!');
                return false;
            }
            var i;
            var codes = [8]; // Восьмой дататайп не используется, но данные для него нужны (массив всех доступных должностей)
            var defs = [];
            var promises = [];
            // Формирование массива типов селекторов для последующего запроса соответствующих им списков значений селекторов
            for (i = 0; i < fields.length; i += 1) {
                if (fields[i].SelectorType !== 0 && codes.indexOf(fields[i].SelectorType) === -1) {
                    codes.push(fields[i].SelectorType);
                }
            }
            // Запрос списков значений и формирование из них массива
            for (i = 0; i < codes.length; i += 1) {
                var code = parseInt(codes[i]);
                defs[code] = $q.defer();
                promises.push(defs[code].promise);
                // Заглушка для отмены запроса массива ОГВ, т.к. он больше не используется для выбора ОГВ
                if (code === 11) {                    
                    defs[code].resolve({ SelectorType: code, values: [] }); // Зарезолвить пустой массив и прервать выполнение итерации
                } else {
                    apiHelper.apiGet('api/SelectorValue/' + code, null, function (result) {
                        defs[result.data.SelectorType].resolve({
                            SelectorType: result.data.SelectorType,
                            values: result.data.Data
                        });
                    });
                }
            }
            return $q.all(promises);
            //var def = $q.deferred();
            //var uri = '/api/SelectorValue/' + selectorType;
            //apiHelper.apiGet(uri, null, null, function (response) {
            //    if (typeof response.data !== 'object') {
            //        def.reject('Ошибка при получении данных с сервера!');
            //    } else {
            //        def.resolve(response.data);
            //    }
            //});
        }

        /**
            * Формирует тело поискового запроса из исходного массива и набора опций
            * @constructor  
            * @param {Array} grid - массив поисковых условий 
            * @param {Object} options - список опций для формирования поискового запроса 
            * @returns {}  
            */
        function Request(grid, options) {
            try {
                if (_.isEmpty(options)) {
                    var sort = {};
                    if (service.tableData.tableState.sort.predicate) {
                        sort[service.tableData.tableState.sort.predicate] = service.tableData.tableState.sort.reverse ? 0 : 1;
                    } 
                    options.sort = sort;
                } // Пустой опшен приходит от кнопок "скачать результат"
                if (options.sort.Name !== undefined) options.sort.PositionForDepartment = 0; // Подмешиваю сортировку по должности
                this.SortOrders = options.sort !== undefined ? options.sort : {}; //{Name:0, PositionForDepartment:0};
                this.Page = options.page !== undefined ? options.page : 1 ;
                this.PageSize = options.pageSize !== undefined ? options.pageSize : 10;
                this.Filters = [];            

                for (var i = 0; i < grid.length; i += 1) {
                    var item = grid[i];
                    var filter = {};
                    filter.Condition = item.condition !== null ? item.condition.Code : 0;
                    filter.SearchCode = item.field !== null ? item.field.SearchCode : 0;
                    filter.DataType = item.field !== null ? item.field.DataType : 0;
                    filter.Operator = item.operator !== null ? item.operator.Code : 0;
                    filter.Value = typeof item.value === 'string' ? item.value : null;                
                    //filter.Value = item.value;
                    filter.Params =
                        item.params ? item.params : null; // Параметры для отдельных критериев
                    this.Filters.push(filter);
                }
                _storage.set('search', 'request', this);
            }
            catch (e) {
                service.tableData.dataReceived = false;
                service.tableData.errorMsg = "Возникла ошибка при формировании запроса"
                $rootScope.$broadcast('personsLoadingError');
                console.warn("Ошибка при формировании запроса!")
                console.log(e);
                _storage.set('search', 'request', null);
                return null;

            }
        }

        function download() {
            var gridCopy = JSON.parse(JSON.stringify(service.request)); // глубокая копия с преобразованием всего в строки
            var request = new Request(gridCopy, {});
            if (!request) return; // Если фозникли проблемы при формировании запроса из таблицы критериев, прервать выполнение
            var hiddenForm = "<form id='dynamicFormTmp' action='apiex/PersonSearch/DownloadFile' method='POST' target='_blank'>"
                                + "<input type='hidden' name='searchRequest' id='searchRequest' value='" + JSON.stringify(request) + "'/ >"
                                + "<button id='submitDownload' type='submit'></button>"
                            + "</form>";

            $cookies.RequestPayload = JSON.stringify(request);
            $('body').append(hiddenForm);
            $timeout(function () {
                $('#submitDownload').click();
                $('#dynamicFormTmp').remove();
            });
            //console.warn(JSON.stringify(request));

        }

        function downloadExp() {
            var gridCopy = JSON.parse(JSON.stringify(service.request)); // глубокая копия с преобразованием всего в строки
            var request = new Request(gridCopy, {});
            if (!request) return; // Если фозникли проблемы при формировании запроса из таблицы критериев, прервать выполнение
            var hiddenForm = "<form id='dynamicFormTmp' action='apiex/PersonSearch/DownloadReport' method='POST' target='_blank'>"
                                + "<input type='hidden' name='searchRequest' id='searchRequest' value='" + JSON.stringify(request) + "'/ >"
                                + "<button id='submitDownload' type='submit'></button>"
                            + "</form>";

            $cookies.RequestPayload = JSON.stringify(request);
            $('body').append(hiddenForm);
            $timeout(function () {
                $('#submitDownload').click();
                $('#dynamicFormTmp').remove();
            });
            //console.warn(JSON.stringify(request));

        }
        /**
            * Реджектит промисы - пока для отладки резолвит
            * @param {deffered object} def - обещание, которое нужно зареджектить
            * @returns {} 
            */
        function setRejectionTimer(def) {
            def.resolve('Time is Out for this Request');
        }
               
        function Dump(obj) {
            var s = "";
            for (var propertyName in obj) {
                s += propertyName + ": " + obj[propertyName];
            }
            return s;
        }
        function restoreRequest() {
            service.savedRequest = _storage.have('search', 'request') ? _storage.get('search', 'request') : null;
        }
        function init() {
            restoreRequest();
            localStorage.setItem('searchTableState', null);   // Сброс настроек таблицы 
        }
    }
})();