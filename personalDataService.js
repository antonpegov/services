﻿(function() {
        'use strict';

        angular
            .module('app.core')
            .factory('personalDataService', personalData);

        personalData.$inject = ['$q', 'apiHelper'];

        function personalData($q, apiHelper) {

            var service = {
                /**
                 * dataType:
                 * null - вложенный массив объектов,
                 * 0 - список,
                 * 1 - таблица
                 */
                personalData: [
                    { name: "Общие сведения", src: "apiex/person/common/", dataType: 0, data: null, method: "get" },
                    {
                        name: "Биография",
                        src: null,
                        subNames: [
                            { name: "Сведения об образовании", src: "apiex/person/education/", dataType: 1, data: null, method: "get" },
                            { name: "Сведения о службе в вооруженных силах", src: "apiex/person/military/", dataType: 1, data: null, method: "get" },
                            { name: "Сведения о семейном положении", src: "apiex/person/family/", dataType: 1, data: null, method: "get" },
                            { name: "Сведения о судимости", src: "apiex/person/conviction/", dataType: 1, data: null, method: "get" }
                        ]
                    },
                    { name: "Занимаемые должности", src: "apiex/person/allpositions/", dataType: 1, data: null, method: "get", sort: ['IsActual', 'DateStart'] },
                    { name: "Поощрения и взыскания", src: "apiex/person/promotion/", dataType: 1, data: null, method: "get" },
                    { name: "Публикации в СМИ", src: "apiex/person/publication/", dataType: 1, data: null, method: "get" },
                    { name: "Сведения о смерти", src: "apiex/person/death/", dataType: 0, data: null, method: "get" }
                ], //: 0 - список, 1 - таблица
                personalAnalytics: [
                    {
                        name: "Показатели деятельности должностного лица",
                        src: null,
                        dataType: 2,
                        subNames: [
                            { name: "Поощрения и взыскания", src: "apiex/person/promotion/", dataType: 3, data: null, method: "get" },
                            { name: "Публикации в СМИ", src: "apiex/person/publication/", dataType: -1, data: null, method: "get" }
                        ]
                    },
                    {
                        name: "Анализ переназначений",
                        src: null,
                        subNames: [{
                            name: "История переназначений",
                            src: "apiex/person/allpositions/",
                            dataType: 2,
                            data: null,
                            method: "get",
                            sort: ['IsActual', 'DateStart']
                        }]
                    },
                    {
                        name: "Связанные должностные лица",
                        src: "apiex/person/connected/",
                        dataType: 4,
                        data: null,
                        method: "get"
                    },
                    {
                        name: "Показатели деятельности отрасли",
                        src: "apiex/person/region_indicators/",
                        dataType: 3,
                        data: null,
                        method: "get"
                    }

                ],
                /**
                 * type:
                 * 0-строка,
                 * 2-дата с возможным значением "по настоящее время" (если следующее поле == true),
                 * 3-boolean,
                 * 4-массив строк
                 * 
                 * Для корректной работы булевое поле "По настоящее время" должно идти сразу после 
                 * поля "Завершение"  и иметь свойство type === null
                 */
                dictionary: [
                    [
                        { name: 'Гражданство', value: 'CountryName', type: 0 },
                        { name: 'Телефон', value: 'Phone', type: 0 },
                        { name: 'Российский паспорт, серия', value: 'RFPassportSeries', type: 0 },
                        { name: 'Российский паспорт, номер', value: 'RFPassportNumber', type: 0 },
                        { name: 'Российский паспорт, когда выдан', value: 'RFPassportDate', type: 1 },
                        { name: 'Российский паспорт, кем выдан', value: 'RFPassportDepartment', type: 0 },
                        { name: 'Номер пенсионного свидетельства', value: 'PensionCertificateNumber', type: 0 },
                        { name: 'СНИЛС', value: 'SNILS', type: 0 },
                        { name: 'ИНН', value: 'INN', type: 0 },
                        { name: 'Дата регистрации по месту жительства', value: 'LivingAddressStartDate', type: 1 },
                        { name: 'Адрес по месту регистрации', value: 'LivingAddress', type: 0 },
                        { name: 'Адрес фактического места жительства', value: 'ActualAddress', type: 0 }
                    ], // Общие сведения
                    [
                        [
                            { name: 'Период обучения, с', value: 'DateStart', type: 10, sort: true },
                            { name: 'По настоящее время', value: 'UntilNow', type: 3 },
                            { name: 'Период обучения, по', value: 'DateEnd', type: 20 },
                            { name: 'Тип образования', value: 'EducationType', type: 5, dictSrc: 'apiex/enums/edutype', dict: [], loadingFlag: false },
                            { name: 'Наименование учебного заведения', value: 'EducationalInstitutionName', type: 0 },
                            { name: 'Местонахождение учебного заведения', value: 'EducationalInstitutionPlace', type: 0 },
                            { name: 'Форма обучения', value: 'EducationalMethod', type: 5, dictSrc: 'apiex/enums/edumethod', dict: [], loadingFlag: false },
                            { name: 'Факультет или специальность', value: 'Faculty', type: 0 },
                            { name: 'Результат обучения', value: 'EducationResult', type: 5, dictSrc: 'apiex/enums/eduresult', dict: [], loadingFlag: false },
                            { name: 'Обучение завершено с отличием', value: 'IsExcellent', type: 3 },
                            { name: 'Дополнительные сведения', value: 'AdditionalInfo', type: 0 }
                        ], // Образование
                        [
                            { name: 'Наименование войсковой части', value: 'MilitaryUnitName', type: 0 },
                            { name: 'Местонахождение войсковой части', value: 'MilitaryUnitPlace', type: 0 },
                            { name: 'Период службы, с', value: 'DateStart', type: 10, sort: true },
                            { name: 'По настоящее время', value: 'UntilNow', type: 3 },
                            { name: 'Период службы, по', value: 'DateEnd', type: 20 },
                            { name: 'Последняя воинская должность', value: 'LastMilitaryPost', type: 0 },
                            { name: 'Тип воинского звания', value: 'LastMilitaryRankType', type: 5, dictSrc: 'apiex/enums/milranktype', dict: [], loadingFlag: false },
                            { name: 'Последнее воинское звание', value: 'LastMilitaryRank', type: 5, dictSrc: 'apiex/enums/milrank', dict: [], loadingFlag: false }
                        ], // Служба в ВС
                        [
                            { name: 'Степень родства', value: 'RelationType', type: 5, dictSrc: 'apiex/enums/relationtype', dict: [], loadingFlag: false },
                            { name: 'Фамилия, имя, отчество', value: 'FullName', type: 0 },
                            { name: 'Дата рождения', value: 'BirthDateStr', type: 0 },
                            { name: 'Брак расторгнут', value: 'Divorced', type: 3 },
                            { name: 'Сведения о расторжении брака', value: 'AdditionalDivorceInfo', type: 0 },
                            { name: 'Факт усыновления', value: 'Adopted', type: 3 },
                            { name: 'Сведения об усыновлении', value: 'AdditionalAdoptionInfo', type: 0 }
                        ], // Родственники
                        [
                            { name: 'Номер статьи УК', value: 'NumberCC', type: 0 },
                            { name: 'Наименование статьи УК', value: 'NameCC', type: 0 },
                            { name: 'Дата вынесения приговора', value: 'JudgementDate', type: 2 },
                            { name: 'Вид наказания', value: 'PunishmentType', type: 0 },
                            { name: 'Судебный орган, вынесший приговор', value: 'JudicialAuthority', type: 0 },
                            { name: 'Судимость погашена', value: 'CriminalRecordRepaid', type: 3 },
                            { name: 'Наличие ограничений на занятие должностей', value: 'Restrictions', type: 3 },
                            { name: 'Описание ограничений на занятие должностей', value: 'RestrictionsInfo', type: 0 }
                        ] // Судимости
                    ], // Биография
                    [
                        { name: 'Текущее назначение', value: 'IsActual', type: 3 },
                        { name: 'Дата назначения', value: 'DateStart', type: 2 },
                        { name: 'Дата увольнения', value: 'DateEnd', type: 2 },
                        { name: 'Государственный орган', value: 'DepartmentName', type: 0 },
                        { name: 'Должность', value: 'Position', type: 0 },
                        { name: 'Телефон', value: 'Phone', type: 0 },
                        { name: 'Факс', value: 'Fax', type: 0 },
                        { name: 'Документ о назначении', value: 'DocName', type: 0 },
                        { name: 'Дата документа о назначении на должность', value: 'DecreeDate', type: 1 },
                        { name: 'Номер документа о назначении на должность', value: 'DecreeNumber', type: 0 },
                        { name: 'Метод занятия должности', value: 'PositionType', type: 5, dictSrc: 'apiex/enums/postype', dict: [], loadingFlag: false },
                        { name: 'Ставка', value: 'Salary', type: 0 },
                        { name: 'Перечень курируемых органов власти', value: 'SupervisingDepartments', type: 4 },
                        { name: 'Длительность испытательного срока', value: 'TrialPeriod', type: 0 },
                        { name: 'Дата окончания испытательного срока', value: 'TrialPeriodEnd', type: 1 },
                        { name: 'Основание увольнения', value: 'FiringReason', type: 0 },
                        { name: 'Дополнительные сведения, касающиеся периода занятия должности', value: 'Description', type: 0 },
                    ], // Занимаемые должности
                    [
                        { name: 'Вид события', value: 'Type', type: 5, dictSrc: 'apiex/enums/prorecoverytype', dict: [], loadingFlag: false},
                        { name: 'Дата события', value: 'Date', type: 2, sort: true },
                        { name: 'Должность', value: 'Position', type: 0 },
                        { name: 'Орган государственной власти', value: 'DepartmentName', type: 0 },
                        { name: 'Дополнительные сведения о событии', value: 'Description', type: 0 }
                    ], // Поощрения
                    [
                        { name: 'Источник информации', value: 'Source', type: 0 },
                        { name: 'Дата публикации', value: 'PublicationDate', type: 2, sort: true },
                        { name: 'Регион', value: 'Region', type: 0 },
                        { name: 'Наименование статьи', value: 'Name', type: 0 },
                        { name: 'Автор', value: 'Author', type: 0 },
                        { name: 'Дополнительные сведения о публикации', value: 'AdditionalInfo', type: 0 },
                        { name: 'Оценка публикации', value: 'Assessment', type: 5, dictSrc: 'apiex/enums/PublicationInfoType', dict: [], loadingFlag: false }
                    ], // Публикации
                    [
                        { name: 'Информация', value: 'Info', type: 0 },
                        { name: 'Дата смерти', value: 'DeathDate', type: 1 }
                        //{ name: 'Источник информации', value: 'Source', type: 0 }
                    ] // Смерть
                ],

                analyticsDictionary: [
                    [],
                    [
                        [
                            { name: 'Наименование государственного органа', value: 'DepartmentName', type: 0 },
                            { name: 'Дата вступления в должность', value: 'DateStart', type: 1 },
                            { name: 'Дата освобождения должности', value: 'DateEnd', type: 2 },
                            { name: 'По настоящее время', value: 'UntilNow', type: null },
                            { name: 'Текущее назначение', value: 'IsActual', type: 3 }
                        ]
                    ],
                    [
                        { name: 'Наименование показателя', value: 'Name', type: 0 },
                        { name: 'Показатель', value: 'Values', type: 0 }
                    ]
                ],

                getPersonalData: getPersonalData,
                fillDictionaryValues: fillDictionaryValues
        }
            return service;

            function fillDictionaryValues(dictItem) {
                apiHelper.apiGet(dictItem.dictSrc, dictItem, function(result, value) {
                    if (_.isArray(result.data)) {
                        _.each(result.data, function(item) {
                            dictItem.dict[item.Id] = item.Name;
                        });
                    } else {
                        console.error('Не могу получить от сервера словарь для %s', dictItem.name);
                    }
                });
            }
            function getPersonalData(id) {
                var deferred = $q.defer();
                var defArray = [];
                var promises = [];
                var defIndex = -1; //Индекс для отслеживания конкретного промиса, 
                var personId = id;
                if (id) {
                    personId = id;
                //} else if (typeof id === 'undefined' && typeof _selection.selected.person !== null) {
                //    personId = _selection.selected.person.Id;
                } else {
                    deferred.reject('Метод getPersonalData(id) не может запросить данные без id!');
                    return deferred.promise;
                }
                loadData(defArray, promises, defIndex, personId, service.personalData);
                loadData(defArray, promises, defArray.length - 1, personId, service.personalAnalytics);
                return $q.all(promises);
            }
            function loadData(defArray, promises, defIndex, personId, serviceData) {
                var payload = null;
                for (var i = 0; i < serviceData.length; i += 1) {
                    var item = serviceData[i];
                    if (item.src !== null) {
                        defIndex += 1;
                        defArray[defIndex] = $q.defer();
                        promises.push(defArray[defIndex].promise);
                        payload = { personId: personId, src: item.src, defIndex: defIndex };
                        if (item.method === "get") {
                            apiHelper.apiGet(item.src + personId, payload, function (result, request) {
                                var item = _.findWhere(serviceData, { src: request.src });
                                var itemIndex = _.indexOf(serviceData, item);
                                if (itemIndex === -1) {
                                    defArray[request.defIndex].reject("Элемент с src = %s не найден", request.src);
                                } else {
                                    serviceData[itemIndex].data = !item.sort
                                            ? (typeof result.data.Total !== 'undefined') ? result.data.Data : result.data
                                            : objSort(result.data, [item.sort[0], true], item.sort[1])
                                            //: result.data.sort(sort_by(item.sort[0], {name: item.sort[1], primer: parseInt, reverse: false}, true));
                                    defArray[request.defIndex].resolve(true);
                                }
                                return;
                            });

                        } else {
                            apiHelper.apiPost(item.src, payload, function(result, request) {
                                var item = _.findWhere(serviceData, { src: request.src });
                                var itemIndex = _.indexOf(serviceData, item);
                                if (itemIndex === -1) {
                                    defArray[request.defIndex].reject("Элемент с src = %s не найден", request.src);
                                } else {
                                    serviceData[itemIndex].data = (typeof result.data.Total !== 'undefined') ? result.data.Data : result.data;
                                    defArray[request.defIndex].resolve(true);
                                }
                                return;
                            });
                        }
                    } else {
                        var subArray = serviceData[_.indexOf(serviceData, item)].subNames;
                        for (var j = 0; j < subArray.length; j += 1) {
                            defIndex += 1;
                            defArray[defIndex] = $q.defer();
                            promises.push(defArray[defIndex].promise);
                            payload = { personId: personId, src: subArray[j].src, defIndex: defIndex, baseIndex: i };
                            if (subArray[j].method === "get") {
                                apiHelper.apiGet(subArray[j].src + personId, payload, function (result, request) {
                                    var item = _.findWhere(serviceData[request.baseIndex].subNames, { src: request.src });
                                    var itemIndex = _.indexOf(serviceData[request.baseIndex].subNames, item);
                                    if (itemIndex === -1) {
                                        console.error("Элемент не найден!");
                                        defArray[request.defIndex].reject("Элемент с src = %s не найден", request.src);
                                    } else {
                                        serviceData[request.baseIndex].subNames[itemIndex].data = !item.sort
                                            ? (typeof result.data.Total !== 'undefined') ? result.data.Data : result.data
                                            : objSort(result.data, [item.sort[0], true], item.sort[1])
                                        //serviceData[request.baseIndex].subNames[itemIndex].data = (typeof result.data.Total !== 'undefined') ? result.data.Data : result.data;
                                        defArray[request.defIndex].resolve(true);
                                    }
                                    return;
                                });

                            } else {
                                apiHelper.apiPost(subArray[j].src, payload, function(result, request) {
                                    var item = _.findWhere(serviceData[request.baseIndex].subNames, { src: request.src });
                                    var itemIndex = _.indexOf(serviceData[request.baseIndex].subNames, item);
                                    if (itemIndex === -1) {
                                        console.error("Элемент не найден!");
                                        defArray[request.defIndex].reject("Элемент с src = %s не найден", request.src);
                                    } else {
                                        serviceData[request.baseIndex].subNames[itemIndex].data = (typeof result.data.Total !== 'undefined') ? result.data.Data : result.data;
                                        defArray[request.defIndex].resolve(true);
                                    }
                                    return;
                                });
                            }
                        }
                    }
                }
            }
            function objSort() {
                var args = arguments,
                    array = args[0],
                    case_sensitive, keys_length, key, desc, a, b, i;

                if (typeof arguments[arguments.length - 1] === 'boolean') {
                    case_sensitive = arguments[arguments.length - 1];
                    keys_length = arguments.length - 1;
                } else {
                    case_sensitive = false;
                    keys_length = arguments.length;
                }

                return array.sort(function (obj1, obj2) {
                    for (i = 1; i < keys_length; i++) {
                        key = args[i];
                        if (typeof key !== 'string') {
                            desc = key[1];
                            key = key[0];
                            a = obj1[args[i][0]];
                            b = obj2[args[i][0]];
                        } else {
                            desc = false;
                            a = obj1[args[i]];
                            b = obj2[args[i]];
                        }

                        if (case_sensitive === false && typeof a === 'string') {
                            a = a.toLowerCase();
                            b = b ? b.toLowerCase() : "1888-01-01t00:00:00";
                        }

                        if (!desc) {
                            if (a < b) return -1;
                            if (a > b) return 1;
                        } else {
                            if (a > b) return -1;
                            if (a < b) return 1;
                        }
                    }
                    return 0;
                });
            } //end of objSort() function


        }
    }
)();
