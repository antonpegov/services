﻿// Сервис для хранения и управления деревом организаций, инжектится в директиву плиток и listService ( ему нужна
// активная организация для загрузки сотрудников в таблицу). Оповещает окружающих об изменении активного элемента 
// генерацией события 'selectionOnTree' с индексом выбранного элемента (-1 означает отмены выбора), а о  начале 
// обновления данных самого дерева событием 'newTree'(false), о готовности - 'newTree'(true). Начинает обновление 
// получив событие 'changeTree' (ОГВ). При начале обновления вызывает обновление стека событием 'changeStack'. 


(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('_tree', factory);

    factory.$inject = ['$rootScope', '$q', '_storage', '_resource', 'apiHelper'];

    function factory($rootScope, $q, _storage, _resource, apiHelper) {
        var busy = false;       // флаг занятости для вложения в бродкаст и запуска-остановки спинера
        var canceller = null;   // содержит промис для отмены предыдущего запроса
        var params = null;      // объект с параметрами запроса, сюда вкладывается отменяющий промис
        var limit = 500;
        var address = 'apiex/govdepartment/find?Id=';
        var service = {
            name: 'treeService',
            module: 'list',
            overloaded: false,
            parent: _storage.have('list','parent') ? _storage.get('list','parent') : null,     
            elements: _storage.have('list','elements') ? _storage.get('list','elements') : [],
            active: _storage.have('list', 'active') ? _storage.get('list', 'active') : null,
            select: select,
            getActiveIndex: getActiveIndex
        };
        
        $rootScope.$on('changeTree', function (event, request) {
            service.active = null; // Без этого возникают проблеммы в быстром поиске
            reloadTree(request);
        });
        // Перезагрузка модели автоматически приводит к открытию модуля "Справочник" и обновлению всех данных в нем
        $rootScope.$on('loadTreeItem', function (event, itemId) {
            openSomeTreeItem(itemId);
        });
        $rootScope.$on('reloadTree', function (event, callback) {
            service.active = null; // Без этого возникают проблеммы в быстром поиске
            reloadTree({ element: service.parent, callback: callback, simple: true});
        });

        init();

        return service;

        /* ========================================================= */
        /*                           Методы                          */
        /* ========================================================= */

        function getActiveIndex() {
            if (!service.active)
                return -1;
            else if (_.isEmpty(service.elements)) {
                console.log('Нет данных для выполнения!');
                return -1;
            }

            return _.findIndex(service.elements, function (item, index) {
                if (item.Id === service.active.Id)
                    return true;
            });
        }
        function select(index, callback) {
            if (!_.isNumber(index) || _.isNumber(index) && index >= service.elements.length) {
                console.warn('Ошибочный параметр');
                return false;
            } // Проверка
            var element = service.elements[index]; // Выбранный элемент дерева
            if (element.ChildCount > 0) {
                index = -1;
                reloadTree({ element: element }); // Выбор промежуточного узла приводит к его загрузке
            } else {
                index = index === service.getActiveIndex() ? -1 : index; // При совпадении выставить в -1
            }
            service.active = index === -1 ? null : element;
            $rootScope.$broadcast('selectionOnTree', index); // Оповестить об изменении выбранного эл-та
            _storage.set(service.module, 'active', service.active); // Зафиксировать в хранилище
            callback && callback();

        }


        /* Приватные функции */
        function openSomeTreeItem(itemId){
            // Проверить, является ли элемент конечным или у него есть наследники
            _resource.getOgvById(itemId, function (govDep) {
                if(!govDep) return;
                if (govDep.ChildCount > 0) {
                    // Если есть наследники - загрузить этот элемент в корень дерева
                    service.active = null; 
                    reloadTree({ element: govDep });
                } else {
                    // Если нет наследников - загрузить в корень дерева родительский элемент и заказать отложенный выбор.
                    _resource.getOgvById(govDep.ParentId, function (parentDep) {
                        service.active = govDep;
                        reloadTree({ element: parentDep });
                    })
                }
            });
        }
        function error(){
            console.error("Не могу получить список организаций от сервера!");
            return []; // Заглушка - при любой ошибке будет возвращаться пустой массив
        }
        function status() {
            $rootScope.$broadcast('newTree', !busy);
        };
        function reloadTree(params) {
            // Подготовка параметров запроса
            var elementId = _.isEmpty(params) || _.isObject(params) && _.isEmpty(params.element) ? null : params.element.Id;
            var searchString = _.isEmpty(params) || _.isObject(params) && _.isEmpty(params.searchString) ? null : params.searchString;
            service.parent = elementId === null ? null : params.element ? params.element : null; // Установка конневого элемента
            // Если нет поисковой строки, то взять elementId из хранилища (случай возврата из поиска по ОГВ, когда service.parent сброшен в null)
            if (elementId === null && searchString === null && !params.moveUp && _storage.have('list', 'parent')) {
                service.parent = _storage.get('list', 'parent');
                elementId = service.parent.Id;
            }
            busy = true; // Флаг для ускорения работы forEach
            status(); // Оповестить окружающих о занятости
            $rootScope.$broadcast('openLeftPanel', true); // Выдвинуть панель с плитками
            $rootScope.$broadcast('changeStack', elementId ? service.parent : null); // Обновить стек родительских элементов
            getElements(elementId, searchString, function (elements) {
                if (!_.isArray(elements)) {
                    console.error("Ответ сервера не соответствует ожидаемому формату!");
                    return;
                }

                if (service.parent != null)
                    service.parent.Items = elements;
                service.overloaded = elements.length >= limit ? true : false;
                service.elements = service.overloaded ? [] : elements;
                if (params.simple === true) {
                    // При простой перезагрузке нужно восстановить выбранный элемент из хранилища
                    var activeItem = _storage.have('list','active') ? _storage.get('list','active') : null;
                    var finished = false;
                    if (activeItem) {
                        angular.forEach(service.elements, function (item, index) {
                            if (!finished) {
                                if (activeItem.Id === item.Id) {
                                    service.select(index);
                                    finished = true;
                                }
                            }
                        });
                    }
                }
                _storage.set(service.module, 'parent', service.parent);
                _storage.set(service.module, 'elements', service.elements);
                _storage.set(service.module, 'active', service.active);
                busy = false;   // Разблокировать сервис
                status();       // Оповестить о новом деореве
                params.callback && params.callback();
            });
        }
        function getElements(govDepId, depName, callback) {
            if (!_.isFunction(callback)) {
                console.error("Колбэк обязателен!");
                return false;
            } // Проверить наличие колбэка
            var uri = address + govDepId; // Вставить в запрос идентификатор узла; 
            canceller && canceller.resolve(); // Отменить предыдущий запрос  
            canceller = $q.defer(); // Создать новый отменяющий промис             
            params = { timeout: canceller.promise }; // Вложить отмняющий промис в параметры запроса
            uri += (depName != null) ? '&depName=' + depName : ''; // Для поиска добавить поисковую строку
            apiHelper.apiGet(uri, params, function (result) {
                var govDeps = result.data;
                canceller = null; // Очистить отменяющий промис после получения результата
                _.isObject(govDeps) ? callback(govDeps) : callback(error());
            });
        }
        function init() {
            // Потребовалось обновлять дерево при каждой загрузке
            reloadTree({ element: service.parent });
            service.parent && _resource.getOgvById(service.parent.Id, function (data) {
                service.parent = !_.isEmpty(data) ? data : service.parent;
            });


        }
    }
})();