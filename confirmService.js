﻿/*
 * Сервис для вывода модального окошка подтверждения действия пользователя
 */

(function () {
    'use strict';

    angular
        .module('app.core')
        .service('_confirm', confirmService);

    confirmService.$inject = ['$modal']; 

    function confirmService($modal) {
        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'SPA/widgets/confirm.html'
        };

        var modalOptions = {
            closeButtonText: 'Отмена',
            actionButtonText: 'Да',
            headerText: 'Требуется подтверждение',
            bodyText: 'Вы уверены?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.size = 'small';
                };
            }

            return $modal.open(tempModalDefaults).result;
        };
    }
})();
