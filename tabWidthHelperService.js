﻿
(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('tabWidthHelper', tabWidthHelper);

    function tabWidthHelper() {
        var helper = { widthCrutch: widthCrutch };

        return helper;

        //костыль для установки ширины контейнера
        function widthCrutch() {
            hideEmptyCols(angular.element(".pdtable .personal-data-table, .bunch-table .personal-data-table"))
            try {
                var limit = 991;
                var windowWidth = $(window).width();
                var tabWidth = windowWidth - $('div.nav').width() - (windowWidth < limit ? 20 : 80);
                $('.pdtable .personal-data-table, .bunch-table .personal-data-table').each(function (index, table) {
                    var headers;
                    table = $(table);
                    headers = $("th", table);
                    if (headers.width() > 250) {
                        //var min_width = table.
                        // Уменьшение всех ячеек
                        setTimeout(function () {
                            headers.css('width', 200);
                            $("td", table).css('width', 200);
                            table.css('min-width', '');
                            table.css('width', '');
                            if (table.width() < tabWidth) {
                                table.css('width', tabWidth - 55);
                                table.css('min-width', tabWidth - 55);
                            }
                        }, 100);
                    } else if (table.width() < tabWidth) {
                        
                        // Растянуть таблицу по ширине страницы
                        setTimeout(function () {
                            if (table.width() > tabWidth) return;
                            table.css('width', tabWidth - 55)
                            table.css('min-width', tabWidth - 55);
                        }, 100);
                    }
                });
                $('.bunch-table').each(function (index, container) {
                    $(container).width(tabWidth - 55); // Ресайз контейнера
                });
                $('div.data.ng-scope').children('div.ng-scope').width(tabWidth - 25); //чтобы не было скроллбара
                //$('div.pdtable.ng-scope').height($('div.data.ng-scope').height() - 10);
            } catch (e) {
                console.log('Видимо, не удалось подключить jQuery');
            }
        }
    }
}
)();