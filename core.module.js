﻿(function () {
    'use strict';

    angular.module('app.core', [
        'app.router',
        'app.api',
        'ngSanitize',
        'slick',
        'ngStorage',
        'ngCookies'
    ]);
    angular.module('app.core')
        .directive('imageonload', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.bind('load', function (event) {
                        var newWidth, newHeight, ratio;
                        var el = event.currentTarget;
                        var parent = angular.element(angular.element(el).closest('span')[0]);
                        var naturalHeight = $(el).naturalHeight();
                        var naturalWidth = $(el).naturalWidth();
                        if (naturalWidth > naturalHeight) {
                            newWidth = parent.width();
                            ratio = naturalWidth / newWidth;
                            angular.element(el).width(newWidth);
                            angular.element(el).height(naturalHeight / ratio);
                        } else {
                            newHeight = parent.height();
                            ratio = naturalHeight / newHeight;
                            angular.element(el).height(newHeight);
                            angular.element(el).width(naturalWidth / ratio);
                        }
                        
                        
                    });
                    element.bind('error', function () {
                        console.error('Ошибка при загрузке фотографии!');
                    });
                }
            };
        });
})();


